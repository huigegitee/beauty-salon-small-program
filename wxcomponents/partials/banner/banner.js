const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    sysWidth:"",
    setting:{},
    imgs:null,
    imgData:'',
    autoplay:'',
  },
  ready:function(){
    let that=this;
    that.setData({
      sysWidth: app.globalData.sysWidth,
      setting:app.globalData.setting,
      autoplay:that.data.data.jsonData.autoplay||''
    });
    console.log("====banner====",that.data.data)
    let imgData = that.data.data.jsonData.images;
    let height=that.data.data.jsonData.height<10?that.data.data.jsonData.height * that.data.sysWidth+"px":that.data.data.jsonData.height+"rpx"
    for(let i=0;i<imgData.length;i++){
        let url=imgData[i].linkUrl||imgData[i].imagePath||'';
        console.log('==url===',url)
        if(url&&url.indexOf("v.qq.com")!=-1){
            imgData[i].type='video'
            imgData[i].jsonData={jsonData:{
                source:url,
                autoPlay:0,
                type:'video',
                id:'videoComponent_'+i,
                height:height,
                width:that.data.data.jsonData.width,
                poster:imgData[i].imageUrl||''
            }}
        }else if(url&&url.indexOf(".mp4")!=-1||url.indexOf(".avi")!=-1||url.indexOf(".wmv")!=-1){
            imgData[i].type='video_other';
            imgData[i].jsonData={jsonData:{
                source:url,
                autoPlay:0,
                type:'video_other',
                id:'videoComponent_'+i,
                height:height,
                width:that.data.data.jsonData.width,
                poster:imgData[i].imageUrl||''
            }}
        }else{
            if(url){
                imgData[i].type='img'
            }
        }
    }
    that.setData({ imgData: imgData })
    console.log("=====banner--data=====", that.data.data,that.data.imgData)
  },
  methods: {
    getDataFun:function(e){
        let that=this;
        console.log("====getDataFun====",e)
        let state=e.detail.state;
        if(state=="play"){
            that.setData({ autoplay: '' })
        }else{
            that.setData({ autoplay: true })
        }
    },
    changeSwiperFun:function(e){
        console.log("===changeSwiperFun===",e);
        let index=e.detail.current;
        let that=this;
        that.triggerEvent('bannerCurIndexFun',{index:index})
        let imgData=that.data.imgData
        let idData='#videoComponent_'+index
        if(imgData[index].autoPlay&&imgData[index].type.indexOf('video')!=-1){
            that.selectComponent(idData).onStartPlay(index)
        }
        for(let i=0;i<imgData.length;i++){
            if(imgData[i].type&&imgData[i].type.indexOf('video')!=-1&&index!=i){
                idData='#videoComponent_'+i
                that.selectComponent(idData).onStopPlay(i)
            }
        }
    },
    imageLoad: function (e) {
      let that=this;
      console.log("=====imageLoad====",e,that.data.data.jsonData.height,that.data.sysWidth)
      let index = e.currentTarget.dataset.index;
      let width = e.detail.width;
      let height = e.detail.height;
      let imgData = that.data.data.jsonData.images;
      let fixedHeght = Number(that.data.data.jsonData.height) * that.data.sysWidth;
      imgData[index].height = fixedHeght;
      imgData[index].width = (width / height ) * fixedHeght;
      that.setData({ imgs: imgData })
      console.log("=====imageLoad====", that.data.imgs)
    },
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})