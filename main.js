import Vue from 'vue';
import App from './App';

// import 14 from "./components/14.vue";
// Vue.component("14", 14);

// import uParse from "./components/gaoyia-parse/parse.vue";
// Vue.component("u-parse", uParse);
// 
Vue.config.productionTip = false;

App.mpType = 'app';

const app = new Vue({
    ...App
});
app.$mount();
