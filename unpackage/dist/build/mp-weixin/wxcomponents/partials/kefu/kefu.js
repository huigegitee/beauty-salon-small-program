const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    display:""
  },
  ready:function(){
    let that=this;
    console.log("客服数据", this.data.data)
    this.setData({
      settingData: app.globalData.setting.platformSetting
    })
    console.log('settingData', this.data.settingData)
  },
  methods: {
    calling: function (e) {
      console.log('====e===',e)
      let phoneNumber = e.currentTarget.dataset.phonenumber
      wx.makePhoneCall({
        phoneNumber: phoneNumber,
        success: function () {
          console.log("拨打电话成功！")
        },
        fail: function () {
          console.log("拨打电话失败！")
        }
      })
    },
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      // 缓存
      try {
        wx.setStorageSync('客服数据', event.currentTarget.dataset.url)
      } catch (e) {
      }
      this.setData({
        display: 'none'
      })
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    },
  },
})