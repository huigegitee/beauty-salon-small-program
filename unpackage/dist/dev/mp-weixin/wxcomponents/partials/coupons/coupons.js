const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: '',
    }
  },
  data: {
    someData: {},
    color: ["#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53", "#FE3D53"]
  },
  ready:function(){
    let that=this;
    app.globalData.consoleFun("=====coupons组件-data=====",[that.data.data])
    if (app.globalData.setting.platformSetting.defaultColor && app.globalData.setting.platformSetting.defaultColor != "") {
      app.globalData.consoleFun("=====coupons组件-defaultColor=====",[app.globalData.setting.platformSetting.defaultColor])
      // 有默认色
      this.setData({
        defaultColor: app.globalData.setting.platformSetting.defaultColor
      })
    }
    else {
      // 没有默认色
      this.setData({
        defaultColor: app.globalData.setting.platformSetting.defaultColor
      })
    }
  },
  methods: {
    //领取优惠券
    click: function (e) {
      console.log(e)
      var i = e.currentTarget.dataset.index
      var data = {
        couponId: '',
        couponSecretCode: '',
        couponSecretPassword: ''
      }
      data.couponId = e.currentTarget.dataset.id
      app.globalData.consoleFun("=====coupons组件-data=====",[data])
      var that = this
      var customIndex = app.globalData.AddClientUrl("/gain_coupon.html", data, 'post')
      console.log("A" + app.globalData.headerPost)
      wx.request({
        url: customIndex.url,
        header: app.globalData.headerPost,
        data: customIndex.params,
        method: 'POST',
        success: function (res) {
         app.globalData.consoleFun("=====coupons组件-success=====",[res])
          // 能领取
          if (res.data.id && res.data.id > 0) {
            if (res.data.newGot == 0 && res.data.otherGot == 0){
              wx.showToast({
                title: '你已经领过了',
                icon: 'success',
                duration: 1000
              })
              var color = that.data.color;
              color[i] = "#E7E7E7"
                  that.setData({ color: color })
            }
            else if (res.data.otherGot == 1) {
              wx.showToast({
                title: '已被别人领取',
                icon: 'success',
                duration: 1000
              })
              var color = that.data.color;
              color[i] = "#E7E7E7"
              that.setData({ color: color })
            }
            else if (res.data.newGot == 0) {
              wx.showToast({
                title: '领取成功',
                icon: 'success',
                duration: 1000
              })
              var color = that.data.color;
              color[i] = "#E7E7E7"
              that.setData({ color: color })
            }
          }
          // 不能领取
          else if (res.data.errcode && res.data.errcode == 0) {
            res = res.relateBean
          }
          else if (res.data.errcode && res.data.errcode != 0) {
            wx.showToast({
              title: res.data.errMsg,
              icon: 'none',
              duration: 1000
            })
          }
        },
        fail: function (res) {
          wx.hideLoading()
          app.globalData.loadFail()
        }
      })
    },
    // 点击更多卷
    clickLink: function () {
      wx.navigateTo({
        url: '../../pages/available_coupons/index',
      })
    },
  },
})