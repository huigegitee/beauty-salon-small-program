const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    sysWidth:"",
    version:''
  },
  ready:function(){
    this.setData({
      sysWidth: app.globalData.sysWidth,
      setting:app.globalData.setting,
      version:app.globalData.version,
    });
  },
  methods: {
    buttom: function () {
      app.globalData.wxLogin(1011)
    },
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})