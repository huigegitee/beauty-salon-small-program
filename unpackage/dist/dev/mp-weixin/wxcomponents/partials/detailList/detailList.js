const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    url: '',
    setting: null,
    indexData: null,
    newsList: null,
    activityPromotion: null,
    unactivityPromotion: null,
    products: null,
    activityPromotionNum: null,
    more_scene: '',
    sysWidth: 320,//图片大小 
    loginUser: null,
    showCount: false,
    focusData: null,
    measurementJson: null,
    byNowParams: {},//购买的参数
    bindType: 'addto', //加入购物车or直接下单
    focusIndex: 0,
    showKefu: false,
    productData: null, // 商品数据 
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.consoleFun("=====detailList组件-tolinkUrl=====",[event.currentTarget.dataset.link])
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    },
    readyPay2: function (e) {
      if (!app.globalData.checkIfLogin()) {
        return
      }
      let productData = this.data.productData
      console.log(productData)
      app.globalData.consoleFun("=====detailList组件-productData=====",[productData])
      let way = e.currentTarget.dataset.way
      if (way == 'cart') {
        if (productData.measures.length == 0) {
          this.addtocart()
        } else {
          this.setData({ bindway: way })
          this.setData({ showCount: true })
          let info = productData.productInfo
          this.byNowParams.productId = info.productId
          this.byNowParams.shopId = info.belongShopId
          this.byNowParams.orderType = 0
          this.setData({ byNowParams: this.byNowParams })
          this.chooseMeasureItem()
        }
      } else {
        this.setData({ bindway: way })
        this.setData({ showCount: true })
        let info = productData.productInfo
        this.byNowParams.productId = info.productId
        this.byNowParams.shopId = info.belongShopId
        this.byNowParams.orderType = 0
        this.setData({ byNowParams: this.byNowParams })
        this.chooseMeasureItem()
      }
    },
    //点击加入购物车或立即下单
    bindBuy: function (e) {
      let index = e.currentTarget.dataset.index;
      let bindBuy = e.currentTarget.dataset.bindbuy;
      let products = this.data.products
      let focusData = products[index]
      this.byNowParams.productId = focusData.id
      this.byNowParams.shopId = focusData.belongShopId
      this.byNowParams.orderType = 0
      this.chooseMeasureItem(focusData)
      app.globalData.consoleFun("=====detailList组件-focusData=====",[focusData])
      this.setData({
        focusData: focusData,
        showCount: true,
        byNowParams: this.byNowParams,
        bindBuy: bindBuy
      })
    },
    buyNow: function () {
      let that=this;
      app.globalData.consoleFun("=====detailList组件-buyNow=====",[that.byNowParams])
      if (!app.globalData.checkShopOpenTime()) {
        return
      }
      if (!app.globalData.checkIfLogin()) {
        return
      }
      if (that.data.bindBuy == 'addto') {
        app.globalData.consoleFun("=====detailList组件-加入购物车=====")
        that.addtocart()
      } else {
        app.globalData.consoleFun("=====detailList组件-立即购买=====")
        that.createOrder22(that.byNowParams)
      }
    },
    /* 加入購物車 */
    addtocart: function (e) {
      console.log(e)
    //   var that = this;
    //   var params = {
    //     cartesianId: '',
    //     productId: '',
    //     shopId: '',
    //     count: '',
    //     type: '',
    //   }
    //   params.productId = this.data.focusData.id
    //   params.shopId = this.data.focusData.belongShopId
    //   params.count = this.byNowParams.itemCount
    //   params.type = 'add'
    //   // this.postParams(params)
    //   var customIndex = app.globalData.AddClientUrl("/change_shopping_car_item.html", params, 'post')
    //   wx.request({
    //     url: customIndex.url,
    //     data: customIndex.params,
    //     header: app.globalData.headerPost,
    //     method: 'POST',
    //     success: function (res) {
    //       console.log('---------------change_shopping_car_item-----------------')
    //       console.log(res.data)
    //       wx.hideLoading()
    //       if (that.data.bindType == 'addto') {
    //         that.setData({ showCount: false })
    //       }
    //       if (res.data.productId && res.data.productId != 0) {
    //         that.setData({
    //           carCount: res.data.totalCarItemCount
    //         })
    //         if (data.count == 0) {
    //           console.log('通过加入购物车来确定购物车里面的商品数量')
    //         } else {
    //           wx.showToast({
    //             title: '加入购物车成功',
    //           })
    //         }
    //       } else {
    //         wx.showToast({
    //           title: res.data.errMsg,
    //           image: '/wxcomponents/images/icons/tip.png',
    //           duration: 3000
    //         })
    //       }
    //     },
    //     fail: function (res) {
    //       wx.hideLoading()
    //       app.globalData.loadFail()
    //     }
    //   })
    // },
    // getCart: function () {
    //   let focusProduct = this.data.products[0]
    //   var params = {}
    //   params.productId = focusProduct.id
    //   params.shopId = focusProduct.belongShopId
    //   params.count = 0
    //   params.type = 'add'
    //   this.postParams(params)
    },
    postParams: function (data) {
   
    },
  },
})