const app = getApp();
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    },
    showType: {
      type: JSON,
      value: '',
    },
    bgColor: {
      type: JSON,
      value: '',
    },
    
  },
  data: {
    someData: {},
    color:'#888',
    intervalTime:null,
  },
   ready:function(){
     let that=this;
     console.log("===========this===========", that.data.receiveData)
     that.setData({ platformSetting: app.globalData.setting.platformSetting })
     console.log("===========this===========", that.data.platformSetting)
     if (that.data.receiveData){
         that.data.intervalTime = setInterval(function () {
         var t1 = that.data.receiveData;
         var d1 = t1.replace(/\-/g, "/");
         var date1 = new Date(d1);
         var totalSecond = parseInt((date1 - new Date()) / 1000);
         var second = totalSecond;
         var day = Math.floor(second / 3600 / 24);
         var dayStr = day.toString();
         if (dayStr.length == 1) dayStr = '0' + dayStr;
         var hr = Math.floor((second - day * 3600 * 24) / 3600);
         var hrStr = hr.toString();
         if (hrStr.length == 1) hrStr = '0' + hrStr;
         var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
         var minStr = min.toString();
         if (minStr.length == 1) minStr = '0' + minStr;
         var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
         var secStr = sec.toString();
         if (secStr.length == 1) secStr = '0' + secStr;
         that.setData({
           countDownDay: dayStr,
           countDownHour: hrStr,
           countDownMinute: minStr,
           countDownSecond: secStr,
         });
         totalSecond--;
         if (totalSecond < 0) {
           clearInterval(that.data.intervalTime);
           that.setData({
             countDownDay: '00',
             countDownHour: '00',
             countDownMinute: '00',
             countDownSecond: '00',
           });
           that.setData({
             color: '#888'
           });
         }else{
           that.setData({
             color: that.data.platformSetting.defaultColor
           });
         }
       }.bind(that), 1000);
     }
   },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    },
    setShowTimeStateFun:function(state){
        let that=this;
        if(state){
            clearInterval(that.data.intervalTime);
        }
    }
  },
})