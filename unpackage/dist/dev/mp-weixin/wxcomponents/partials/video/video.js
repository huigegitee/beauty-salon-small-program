const app = getApp();
var part_urls = {};
var videoPage;
var pageArr = new Array()
const TxvContext = requirePlugin("tencentvideo");
const config = require('../../public/config')
Component({
  properties: {


    // 这里定义了innerText属性，属性值可以在组件使用时指定
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    // 这里是一些组件内部数据
    vid: '',
    changingvid: '',
    controls: true,
    autoplay: true,
    posterState:true,
    playState: '',
    showProgress1: true,
    showMuteBtn:true,//显示音量按钮
    isNeedMutex:true,
    isHiddenStop:true,//消失时停止播放
    muted:true,//静音播放
    width: "100%",
    file_id:null,
    poster:null,
    height: "auto",
    videoContext:'',
    videoState:false,
    txvId:'',//'txv'+Math.random().toString(16).substring(2),
  },
  ready: function () {
    var that = this;
    console.log('=====readyvideo====', that.data.data,that.data.txvId)
    that.setData({
      autoplay: that.data.data.jsonData&&that.data.data.jsonData.autoPlay == 0 ? false :true,
      poster: that.data.data.jsonData.poster||'',
      height: that.data.data.jsonData.height||'auto'
    })
    let url = that.data.data.jsonData.source;
    if (url.indexOf('https://v.qq.com/')!=-1){
      console.log('====1====')
     url = url.match(/https:\/\/v.qq.com\/x\/(\S*).html/)[1]
     console.log('url===', url)
     let urlArray = url.split('/')
     console.log('urlArray===', urlArray)
     that.setData({ vid: urlArray[urlArray.length - 1] })
      // 处理视频
      if (that.data.vid != undefined) {
        that.setData({
          file_id: that.data.vid,
          txvId:'txv'+that.data.vid
        });
      } else {
        wx.showToast({
          title: '未传入视频id',
        })
      }
    } else {
      console.log('====2====')
      if(that.data.data.jsonData.type=='video_other'){
          // that.videoContext = wx.createVideoContext('video_'+that.data.data.jsonData.id)
          that.videoContext = wx.createVideoContext(`video_${that.data.data.jsonData.id}`,that)
          console.log("===that.videoContext==",that.videoContext)
      }
      // wx.showToast({
      //   title: '您这个不是腾讯链接~',
      // })
   }
  },
  methods: {
    // 这里是一个自定义方法
    onStateChange:function(){
     let that=this
      if (that.data.data.jsonData.poster) {
        posterState: true
      }
    },
    timeupdateFun:function(e){
        // console.log("==timeupdateFun===",e)
        let that=this;
    },
    onFullScreenChange: function () {

    },
    playFun:function(e){
        let that=this;
        // console.log("===playFun===",)
        that.triggerEvent('sendDataFun',{state:'play'}) 
    },
    pauseFun:function(e){
        let that=this;
        // console.log("===pauseFun===",)
        that.triggerEvent('sendDataFun',{state:'pause'}) 
    },
    onStopPlay:function(e){
        let that=this;
        // console.log("==onStopPlay==",e,that.data.txvId,that.data.data.jsonData.type)
        if(that.data.data.jsonData.type=='video'){
            let txvContext = TxvContext.getTxvContext(that.data.txvId)
            txvContext.pause(); // 暂停
        }else{
            setTimeout(function(){
                // console.log("video_other",that.videoContext,that.data.data.jsonData.id)
                that.videoContext.pause(); // 暂停
            },200)
        }
    },
    onStartPlay:function(e){
        let that=this;
        // console.log("==onStartPlay==",e,that.data.txvId,that.data.data.jsonData.type)
        if(that.data.data.jsonData.type=='video'){
            let txvContext = TxvContext.getTxvContext(that.data.txvId) 
            txvContext.play();  // 播放
        }else{
            setTimeout(function(){
                // console.log("video_other",that.videoContext,that.data.data.jsonData.id)
                that.videoContext.play(); // 播放
            },200)
        }
    },
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      app.globalData.linkEvent(event.currentTarget.dataset.link);

    },
   
  },
})